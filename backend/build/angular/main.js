(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.activity-crud.html":
/*!****************************************!*\
  !*** ./src/app/app.activity-crud.html ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"form-container\">\r\n  <form action=\"\" class=\"form\">\r\n    <div class=\"form-input-container\">\r\n      <label class=\"form-label input-label\">Name:</label>\r\n      <input class=\"text-input\" type=\"text\" placeholder=\"Enter the activity name...\" [formControl]=\"textField\" />\r\n      <small class=\"input-description\">Enter the desired activity name which will be shown on the Activities list.</small>\r\n    </div>\r\n    <div class=\"form-input-container checkbox-container\">\r\n      <input class=\"form-checkbox\" id=\"activityCompleted\" type=\"checkbox\" [formControl]=\"completedField\" />\r\n      <label class=\"form-label checkbox-label\" for=\"activityCompleted\">Activity Completed</label>\r\n    </div>\r\n    <div class=\"form-input-container actions-container\">\r\n      <div class=\"button-container\">\r\n        <input type=\"submit\" class=\"button-primary primary\" value=\"Save\" (click)=\"formSubmit()\" />\r\n      </div>\r\n      <div class=\"button-container\">\r\n        <input type=\"submit\" class=\"button-primary danger\" value=\"Cancel\" (click)=\"formCancel()\" />\r\n      </div>\r\n    </div>\r\n  </form>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/app.activity-crud.ts":
/*!**************************************!*\
  !*** ./src/app/app.activity-crud.ts ***!
  \**************************************/
/*! exports provided: ActivityCrud */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActivityCrud", function() { return ActivityCrud; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _redux_actions_action_response_actions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../redux/actions/action-response.actions */ "./src/redux/actions/action-response.actions.ts");





var ActivityCrud = /** @class */ (function () {
    function ActivityCrud(store) {
        this.store = store;
        this.closeCrud = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.formAction = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.title = 'activity-crud';
        this.textField = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]);
        this.completedField = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]);
    }
    ActivityCrud.prototype.ngOnInit = function () {
    };
    ActivityCrud.prototype.formSubmit = function () {
        var formData = {};
        if (this.textField.valid && this.textField.value.trim().length > 3) {
            formData = {
                name: this.textField.value,
                completed: (this.completedField.value == true) ? this.completedField.value : false
            };
            this.textField.setValue('', { emitEvent: false });
            this.completedField.setValue('', { emitEvent: false });
        }
        this.formAction.emit(formData);
    };
    ActivityCrud.prototype.formCancel = function () {
        this.closeCrud.emit();
        var actResObj = null;
        var toggleNotification = new _redux_actions_action_response_actions__WEBPACK_IMPORTED_MODULE_4__["ToggleAction"](actResObj);
        this.store.dispatch(toggleNotification);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ActivityCrud.prototype, "closeCrud", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ActivityCrud.prototype, "formAction", void 0);
    ActivityCrud = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'activity-crud',
            template: __webpack_require__(/*! ./app.activity-crud.html */ "./src/app/app.activity-crud.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngrx_store__WEBPACK_IMPORTED_MODULE_3__["Store"]])
    ], ActivityCrud);
    return ActivityCrud;
}());



/***/ }),

/***/ "./src/app/app.activity-item.html":
/*!****************************************!*\
  !*** ./src/app/app.activity-item.html ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"activity\">\r\n  <div class=\"activity-details\">\r\n    <div class=\"activity-checkbox delete\" (click)=\"deleteActivity()\">\r\n      <img class=\"activity-checkbox-image\" src=\"../assets/img/delete.svg\" alt=\"completed icon\">\r\n    </div>\r\n    <div class=\"activity-name\">{{activity.name}}</div>\r\n    <div class=\"activity-checkbox change-state\" (click)=\"changeActivityState()\">\r\n      <img class=\"activity-checkbox-image\" *ngIf=\"activity.completed\" src=\"../assets/img/checked.svg\" alt=\"completed icon\">\r\n      <img class=\"activity-checkbox-image\" *ngIf=\"!activity.completed\" src=\"../assets/img/unchecked.svg\" alt=\"completed icon\">\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/app.activity-item.ts":
/*!**************************************!*\
  !*** ./src/app/app.activity-item.ts ***!
  \**************************************/
/*! exports provided: ActivityItem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActivityItem", function() { return ActivityItem; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _redux_actions_todo_actions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../redux/actions/todo.actions */ "./src/redux/actions/todo.actions.ts");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");




//define base component configuration
var ActivityItem = /** @class */ (function () {
    function ActivityItem(
    //define connection to global store
    store) {
        this.store = store;
        //define base component data and map data received from parent component
        this.title = 'activity-item';
    }
    ActivityItem.prototype.ngOnInit = function () {
        //define extra functions and actions that should happen when the component initializes
    };
    ActivityItem.prototype.changeActivityState = function () {
        //invoke store action for changing activity completion status
        this.store.dispatch(new _redux_actions_todo_actions__WEBPACK_IMPORTED_MODULE_2__["ToggleTodoAction"](this.activity));
    };
    ActivityItem.prototype.deleteActivity = function () {
        //invoke store action for deleting the activity from the list
        this.store.dispatch(new _redux_actions_todo_actions__WEBPACK_IMPORTED_MODULE_2__["DeleteTodoAction"](this.activity));
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ActivityItem.prototype, "activity", void 0);
    ActivityItem = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'activity-item',
            template: __webpack_require__(/*! ./app.activity-item.html */ "./src/app/app.activity-item.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngrx_store__WEBPACK_IMPORTED_MODULE_3__["Store"]])
    ], ActivityItem);
    return ActivityItem;
}());



/***/ }),

/***/ "./src/app/app.activity-list.html":
/*!****************************************!*\
  !*** ./src/app/app.activity-list.html ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"activity-list-container\">\r\n  <div class=\"activity-list-details\">\r\n    <h3 class=\"list-title\">ToDo List</h3>\r\n    <div class=\"activity-stats-container\">\r\n      <div class=\"activity-stat\">\r\n        Total: {{total$}}\r\n      </div>\r\n      <div class=\"activity-stat\">\r\n        Completed: {{completed$}}\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"activity-list\" *ngIf=\"total$ > 0\">\r\n    <activity-item *ngFor=\"let activity of activities$\" [activity]=\"activity\"></activity-item>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/app.activity-list.ts":
/*!**************************************!*\
  !*** ./src/app/app.activity-list.ts ***!
  \**************************************/
/*! exports provided: ActivityList */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActivityList", function() { return ActivityList; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");



//define base component configuration
var ActivityList = /** @class */ (function () {
    function ActivityList(store) {
        var _this = this;
        this.store = store;
        //define base component data
        this.title = 'activity-list';
        this.completed$ = 0;
        this.total$ = 0;
        //subscribe to changes in the "todosList" store data
        store.pipe(Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_2__["select"])('todos')).subscribe(function (todosList) {
            //add values to previously defined component data
            _this.activities$ = todosList;
            _this.total$ = todosList.length;
            _this.completed$ = todosList.filter(function (val) { return val.completed === true; }).length;
            //all values defined here will be updated when the data inside "todosList" changes (updates)
        });
    }
    ActivityList.prototype.ngOnInit = function () {
        //define extra functions and actions that should happen when the component initializes
    };
    ActivityList = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'activity-list',
            template: __webpack_require__(/*! ./app.activity-list.html */ "./src/app/app.activity-list.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"]])
    ], ActivityList);
    return ActivityList;
}());



/***/ }),

/***/ "./src/app/app.app.html":
/*!******************************!*\
  !*** ./src/app/app.app.html ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\r\n<app-main></app-main>\r\n"

/***/ }),

/***/ "./src/app/app.app.ts":
/*!****************************!*\
  !*** ./src/app/app.app.ts ***!
  \****************************/
/*! exports provided: AppRoot */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoot", function() { return AppRoot; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppRoot = /** @class */ (function () {
    function AppRoot() {
        this.title = 'angular-todo';
    }
    AppRoot = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.app.html */ "./src/app/app.app.html")
        })
    ], AppRoot);
    return AppRoot;
}());



/***/ }),

/***/ "./src/app/app.header.html":
/*!*********************************!*\
  !*** ./src/app/app.header.html ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header>\r\n  <div class=\"header-container\">\r\n    <div class=\"logo-container\">\r\n      <img class=\"logo\" src=\"../assets/img/logo.png\" alt=\"Angular Logo\">\r\n    </div>\r\n    <div class=\"title-container\">\r\n      <h3>Angular ToDo App</h3>\r\n    </div>\r\n  </div>\r\n</header>\r\n"

/***/ }),

/***/ "./src/app/app.header.ts":
/*!*******************************!*\
  !*** ./src/app/app.header.ts ***!
  \*******************************/
/*! exports provided: AppHeader */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppHeader", function() { return AppHeader; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppHeader = /** @class */ (function () {
    function AppHeader() {
        this.title = 'app-header';
    }
    AppHeader = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./app.header.html */ "./src/app/app.header.html")
        })
    ], AppHeader);
    return AppHeader;
}());



/***/ }),

/***/ "./src/app/app.main.html":
/*!*******************************!*\
  !*** ./src/app/app.main.html ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-container\">\r\n  <div class=\"app-container\">\r\n    <div class=\"notification-dialog\" *ngIf=\"(actionResponse$.error && !actionResponse$.success) || (!actionResponse$.error && actionResponse$.success)\" [ngClass]=\"{'error': (actionResponse$.error && !actionResponse$.success), 'success': (!actionResponse$.error && actionResponse$.success)}\">\r\n        <button type=\"button\" aria-label=\"Close\" class=\"close\" (click)=\"clearAlert()\">×</button>\r\n        {{actionResponse$.notice}}\r\n    </div>\r\n    <div class=\"menu-container\">\r\n      <div class=\"menu-options\">\r\n        <div class=\"option-toggle\">\r\n          <input type=\"submit\" class=\"button-primary primary\" value=\"Add New\" (click)=\"toggleMenu('activityCrud')\">\r\n        </div>\r\n        <div class=\"option-toggle\">\r\n          <input type=\"submit\" class=\"button-primary primary\" value=\"Special\" (click)=\"toggleMenu('specialOptions')\">\r\n        </div>\r\n      </div>\r\n      <div class=\"submenu-container\" *ngIf=\"menuOpen == 'activityCrud'\">\r\n        <div class=\"submenu\">\r\n          <activity-crud (closeCrud)=\"toggleMenu()\"></activity-crud>\r\n        </div>\r\n      </div>\r\n      <div class=\"submenu-container\" *ngIf=\"menuOpen == 'specialOptions'\">\r\n        <div class=\"submenu\">\r\n          <div class=\"form-container\">\r\n            <form action=\"\" class=\"form\">\r\n              <div class=\"form-input-container\">\r\n                <label class=\"form-label input-label\">Options:</label>\r\n                <div class=\"select-container\">\r\n                  <select class=\"form-select\" (change)=\"selectSpecial($event.target.value)\">\r\n                    <option class=\"form-option\" *ngFor=\"let spec of specials$\" [value]=\"spec.value\" [selected]=\"spec.value == special$\" [disabled]=\"spec.value === null\">\r\n                      {{spec.text}}\r\n                    </option>\r\n                  </select>\r\n                </div>\r\n              </div>\r\n              <div class=\"form-input-container actions-container\">\r\n                <div class=\"button-container\">\r\n                  <input type=\"submit\" class=\"button-primary primary\" value=\"Save\" (click)=\"specialSubmit()\">\r\n                </div>\r\n                <div class=\"button-container\">\r\n                  <input type=\"submit\" class=\"button-primary danger\" value=\"Cancel\" (click)=\"specialCancel()\">\r\n                </div>\r\n              </div>\r\n            </form>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <activity-list></activity-list>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/app.main.ts":
/*!*****************************!*\
  !*** ./src/app/app.main.ts ***!
  \*****************************/
/*! exports provided: AppMain */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppMain", function() { return AppMain; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _redux_actions_action_response_actions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../redux/actions/action-response.actions */ "./src/redux/actions/action-response.actions.ts");
/* harmony import */ var _redux_actions_todo_actions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../redux/actions/todo.actions */ "./src/redux/actions/todo.actions.ts");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_5__);






axios__WEBPACK_IMPORTED_MODULE_5___default.a.defaults.headers.get['Content-Type'] = 'multipart/form-data;';
axios__WEBPACK_IMPORTED_MODULE_5___default.a.defaults.headers.get['Access-Control-Allow-Origin'] = '*';
var AppMain = /** @class */ (function () {
    function AppMain(store) {
        var _this = this;
        this.store = store;
        this.title = 'angular-todo';
        this.menuOpen = '';
        this.special$ = null;
        this.specials$ = [
            {
                text: 'Please select an option...',
                value: null
            },
            {
                text: 'Add 100',
                value: 100
            },
            {
                text: 'Add 1000',
                value: 1000
            },
            {
                text: 'Remove 100',
                value: -100
            },
            {
                text: 'Remove 1000',
                value: -1000
            },
            {
                text: 'Fetch 1.000',
                value: 'fetch_1000'
            },
            {
                text: 'Fetch 10.000',
                value: 'fetch_10000'
            },
            {
                text: 'Delete All',
                value: 0
            }
        ];
        store.pipe(Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_2__["select"])('actionResponse')).subscribe(function (val) { return _this.actionResponse$ = val; });
        store.pipe(Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_2__["select"])('todos')).subscribe(function (val) { return _this.todos$ = val; });
    }
    AppMain.prototype.ngOnInit = function () {
        //console.log(this._store);
    };
    AppMain.prototype.toggleMenu = function (option) {
        if (option == undefined || option == this.menuOpen)
            this.menuOpen = '';
        else
            this.menuOpen = option;
        this.special$ = null;
    };
    AppMain.prototype.clearAlert = function () {
        var actResObj = null;
        this.store.dispatch(new _redux_actions_action_response_actions__WEBPACK_IMPORTED_MODULE_3__["ToggleAction"](actResObj));
    };
    AppMain.prototype.selectSpecial = function (value) {
        this.special$ = value;
    };
    AppMain.prototype.specialCancel = function () {
        this.toggleMenu(undefined);
        this.special$ = null;
    };
    AppMain.prototype.toggleResponse = function (responseDataIn) {
        var responseData = responseDataIn;
        this.store.dispatch(new _redux_actions_action_response_actions__WEBPACK_IMPORTED_MODULE_3__["ToggleAction"](responseData));
    };
    AppMain.prototype.formAction = function (data) {
        var responseData = null;
        if (data.name != undefined && data.completed != undefined) {
            this.store.dispatch(new _redux_actions_todo_actions__WEBPACK_IMPORTED_MODULE_4__["AddTodoAction"](data.name, data.completed));
            responseData = {
                error: false,
                notice: 'Entered new activity!',
                success: true,
                inputError: false
            };
        }
        else {
            responseData = {
                error: true,
                notice: 'Please enter the activity name with at least 3 characters.',
                success: false,
                inputError: true
            };
        }
        this.toggleResponse(responseData);
    };
    AppMain.prototype.fetchActivities = function (url) {
        var _this = this;
        var activityNum = (url == 'fetchThousand') ? '1.000' : '10.000';
        axios__WEBPACK_IMPORTED_MODULE_5___default.a.get('http://localhost:8000/api/' + url).then(function (res) {
            _this.store.dispatch(new _redux_actions_todo_actions__WEBPACK_IMPORTED_MODULE_4__["AddTodosAction"](res.data.data));
            _this.toggleResponse({ error: false, notice: 'Fetched ' + activityNum + ' activities.', success: true });
        });
    };
    AppMain.prototype.specialSubmit = function () {
        if (this.special$ !== '') {
            var len = 0, numTemp = 0;
            if (this.special$ === 'fetch_1000') {
                this.fetchActivities('fetchThousand');
            }
            else if (this.special$ === 'fetch_10000') {
                this.fetchActivities('fetchTenThousand');
            }
            else if (this.special$ === '0') {
                this.store.dispatch(new _redux_actions_todo_actions__WEBPACK_IMPORTED_MODULE_4__["ResetTodosAction"]());
                this.toggleResponse({ error: false, notice: 'Activities have been reset.', success: true });
            }
            else if (this.special$ > 0) {
                len = this.todos$.length;
                numTemp = this.special$;
                while (numTemp > 0) {
                    var todo = {
                        name: 'Test activity ' + len++,
                        completed: false
                    };
                    this.store.dispatch(new _redux_actions_todo_actions__WEBPACK_IMPORTED_MODULE_4__["AddTodoAction"](todo.name, todo.completed));
                    numTemp--;
                }
                this.toggleResponse({ error: false, notice: 'Added ' + this.special$ + ' activities.', success: true });
            }
            else if (this.special$ < 0) {
                var start = this.special$ * (-1);
                len = this.todos$.length;
                numTemp = 0;
                if (start > len)
                    start = len;
                else {
                    numTemp = len - start;
                }
                if (start > 0) {
                    this.store.dispatch(new _redux_actions_todo_actions__WEBPACK_IMPORTED_MODULE_4__["DeleteTodoSAction"](numTemp, start));
                    this.toggleResponse({ error: false, notice: 'Deleted ' + start + ' activities.', success: true });
                }
                else {
                    this.toggleResponse({ error: true, notice: 'Activity list is empty, nothing to delete!', success: false });
                }
            }
        }
    };
    AppMain = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-main',
            template: __webpack_require__(/*! ./app.main.html */ "./src/app/app.main.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"]])
    ], AppMain);
    return AppMain;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _redux_app_reducer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../redux/app.reducer */ "./src/redux/app.reducer.ts");
/* harmony import */ var _ngrx_store_devtools__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ngrx/store-devtools */ "./node_modules/@ngrx/store-devtools/fesm5/store-devtools.js");
/* harmony import */ var _app_app__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.app */ "./src/app/app.app.ts");
/* harmony import */ var _app_main__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app.main */ "./src/app/app.main.ts");
/* harmony import */ var _app_header__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./app.header */ "./src/app/app.header.ts");
/* harmony import */ var _app_activity_crud__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./app.activity-crud */ "./src/app/app.activity-crud.ts");
/* harmony import */ var _app_activity_list__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./app.activity-list */ "./src/app/app.activity-list.ts");
/* harmony import */ var _app_activity_item__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./app.activity-item */ "./src/app/app.activity-item.ts");













var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            declarations: [
                _app_header__WEBPACK_IMPORTED_MODULE_9__["AppHeader"],
                _app_app__WEBPACK_IMPORTED_MODULE_7__["AppRoot"],
                _app_main__WEBPACK_IMPORTED_MODULE_8__["AppMain"],
                _app_activity_crud__WEBPACK_IMPORTED_MODULE_10__["ActivityCrud"],
                _app_activity_list__WEBPACK_IMPORTED_MODULE_11__["ActivityList"],
                _app_activity_item__WEBPACK_IMPORTED_MODULE_12__["ActivityItem"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _ngrx_store__WEBPACK_IMPORTED_MODULE_4__["StoreModule"].forRoot(_redux_app_reducer__WEBPACK_IMPORTED_MODULE_5__["rootReducer"]),
                _ngrx_store_devtools__WEBPACK_IMPORTED_MODULE_6__["StoreDevtoolsModule"].instrument({
                    maxAge: 10
                })
            ],
            providers: [],
            bootstrap: [_app_app__WEBPACK_IMPORTED_MODULE_7__["AppRoot"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ "./src/redux/actions/action-response.actions.ts":
/*!******************************************************!*\
  !*** ./src/redux/actions/action-response.actions.ts ***!
  \******************************************************/
/*! exports provided: TOGGLE_RESPONSE, DELETE_TODO, DeleteTodoResponseAction, ToggleAction */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TOGGLE_RESPONSE", function() { return TOGGLE_RESPONSE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DELETE_TODO", function() { return DELETE_TODO; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeleteTodoResponseAction", function() { return DeleteTodoResponseAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToggleAction", function() { return ToggleAction; });
var TOGGLE_RESPONSE = '[ACTIONRESPONSE] toggle';
var DELETE_TODO = '[TODO] delete';
var DeleteTodoResponseAction = /** @class */ (function () {
    function DeleteTodoResponseAction() {
        this.type = DELETE_TODO;
    }
    return DeleteTodoResponseAction;
}());

var ToggleAction = /** @class */ (function () {
    function ToggleAction(response) {
        this.response = response;
        this.type = TOGGLE_RESPONSE;
    }
    return ToggleAction;
}());



/***/ }),

/***/ "./src/redux/actions/todo.actions.ts":
/*!*******************************************!*\
  !*** ./src/redux/actions/todo.actions.ts ***!
  \*******************************************/
/*! exports provided: ADD_TODO, ADD_TODOS, DELETE_TODO, DELETE_TODOS, TOGGLE_TODO, RESET_TODOS, GENERATE_ACTIVITIES, AddTodoAction, AddTodosAction, DeleteTodoAction, DeleteTodoSAction, ToggleTodoAction, ResetTodosAction, GenerateActivitiesAction */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ADD_TODO", function() { return ADD_TODO; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ADD_TODOS", function() { return ADD_TODOS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DELETE_TODO", function() { return DELETE_TODO; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DELETE_TODOS", function() { return DELETE_TODOS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TOGGLE_TODO", function() { return TOGGLE_TODO; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RESET_TODOS", function() { return RESET_TODOS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GENERATE_ACTIVITIES", function() { return GENERATE_ACTIVITIES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddTodoAction", function() { return AddTodoAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddTodosAction", function() { return AddTodosAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeleteTodoAction", function() { return DeleteTodoAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeleteTodoSAction", function() { return DeleteTodoSAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToggleTodoAction", function() { return ToggleTodoAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResetTodosAction", function() { return ResetTodosAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GenerateActivitiesAction", function() { return GenerateActivitiesAction; });
var ADD_TODO = '[TODO] add';
var ADD_TODOS = '[TODO] add multiple';
var DELETE_TODO = '[TODO] delete';
var DELETE_TODOS = '[TODO] delete multiple';
var TOGGLE_TODO = '[TODO] toggle';
var RESET_TODOS = '[TODO] clear all';
var GENERATE_ACTIVITIES = '[TODO] generate activities';
var AddTodoAction = /** @class */ (function () {
    function AddTodoAction(name, completed) {
        this.name = name;
        this.completed = completed;
        this.type = ADD_TODO;
    }
    return AddTodoAction;
}());

var AddTodosAction = /** @class */ (function () {
    function AddTodosAction(data) {
        this.data = data;
        this.type = ADD_TODOS;
    }
    return AddTodosAction;
}());

var DeleteTodoAction = /** @class */ (function () {
    function DeleteTodoAction(todo) {
        this.todo = todo;
        this.type = DELETE_TODO;
    }
    return DeleteTodoAction;
}());

var DeleteTodoSAction = /** @class */ (function () {
    function DeleteTodoSAction(num, start) {
        this.num = num;
        this.start = start;
        this.type = DELETE_TODOS;
    }
    return DeleteTodoSAction;
}());

var ToggleTodoAction = /** @class */ (function () {
    function ToggleTodoAction(todo) {
        this.todo = todo;
        this.type = TOGGLE_TODO;
    }
    return ToggleTodoAction;
}());

var ResetTodosAction = /** @class */ (function () {
    function ResetTodosAction() {
        this.type = RESET_TODOS;
    }
    return ResetTodosAction;
}());

var GenerateActivitiesAction = /** @class */ (function () {
    function GenerateActivitiesAction() {
        this.type = GENERATE_ACTIVITIES;
    }
    return GenerateActivitiesAction;
}());



/***/ }),

/***/ "./src/redux/app.reducer.ts":
/*!**********************************!*\
  !*** ./src/redux/app.reducer.ts ***!
  \**********************************/
/*! exports provided: rootReducer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "rootReducer", function() { return rootReducer; });
/* harmony import */ var _reducers_todo_reducer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./reducers/todo.reducer */ "./src/redux/reducers/todo.reducer.ts");
/* harmony import */ var _reducers_action_response_reducer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./reducers/action-response.reducer */ "./src/redux/reducers/action-response.reducer.ts");


var rootReducer = {
    todos: _reducers_todo_reducer__WEBPACK_IMPORTED_MODULE_0__["TodosReducer"],
    actionResponse: _reducers_action_response_reducer__WEBPACK_IMPORTED_MODULE_1__["ActionResponseReducer"]
};


/***/ }),

/***/ "./src/redux/reducers/action-response.reducer.ts":
/*!*******************************************************!*\
  !*** ./src/redux/reducers/action-response.reducer.ts ***!
  \*******************************************************/
/*! exports provided: ActionResponseReducer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActionResponseReducer", function() { return ActionResponseReducer; });
/* harmony import */ var _actions_action_response_actions__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../actions/action-response.actions */ "./src/redux/actions/action-response.actions.ts");

var initialState = { error: false, notice: '', success: false, inputError: false };
function ActionResponseReducer(state, action) {
    if (state === void 0) { state = initialState; }
    switch (action.type) {
        case _actions_action_response_actions__WEBPACK_IMPORTED_MODULE_0__["TOGGLE_RESPONSE"]: {
            return (action.response == null) ? { error: false, notice: '', success: false, inputError: false } : action.response;
        }
        case _actions_action_response_actions__WEBPACK_IMPORTED_MODULE_0__["DELETE_TODO"]: {
            return { error: false, notice: 'Activity deleted.', success: true, inputError: false };
        }
        default: {
            return state;
        }
    }
}


/***/ }),

/***/ "./src/redux/reducers/todo.reducer.ts":
/*!********************************************!*\
  !*** ./src/redux/reducers/todo.reducer.ts ***!
  \********************************************/
/*! exports provided: TodosReducer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TodosReducer", function() { return TodosReducer; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _actions_todo_actions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../actions/todo.actions */ "./src/redux/actions/todo.actions.ts");


var initialState = [{ name: 'Hand in thesis', completed: true }, { name: 'Prepare thesis defense', completed: true }, { name: 'Obtain title "stručni specijalist inženjer informacijske tehnologije"', completed: false }];
function TodosReducer(state, action) {
    if (state === void 0) { state = initialState; }
    switch (action.type) {
        case _actions_todo_actions__WEBPACK_IMPORTED_MODULE_1__["ADD_TODO"]: {
            return state.concat([
                {
                    name: action.name,
                    completed: action.completed
                }
            ]);
        }
        case _actions_todo_actions__WEBPACK_IMPORTED_MODULE_1__["ADD_TODOS"]: {
            return state.concat(action.data);
        }
        case _actions_todo_actions__WEBPACK_IMPORTED_MODULE_1__["TOGGLE_TODO"]: {
            return state.map(function (todo) {
                return (todo === action.todo)
                    ? tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, todo, { completed: !todo.completed }) : todo;
            });
        }
        case _actions_todo_actions__WEBPACK_IMPORTED_MODULE_1__["DELETE_TODO"]: {
            return state.filter(function (val) { return val !== action.todo; });
        }
        case _actions_todo_actions__WEBPACK_IMPORTED_MODULE_1__["DELETE_TODOS"]: {
            return state.splice(action.start, action.num);
        }
        case _actions_todo_actions__WEBPACK_IMPORTED_MODULE_1__["RESET_TODOS"]: {
            return [];
        }
        case _actions_todo_actions__WEBPACK_IMPORTED_MODULE_1__["GENERATE_ACTIVITIES"]: {
            return state;
        }
        default: {
            return state;
        }
    }
}


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\work_personal\js-framework-comparison\frontend\angular-todo\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map