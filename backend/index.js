const express = require('express')
const path = require('path')
const cors = require('cors')
const axios = require('axios')
const config = require('config')
const app = express()
const port = 8000
const fs = require('fs')

var thousand = [],
  tenThousand = [];

app.use(cors(
{
    credentials: true,
    origin: true
}))

app.use('/', express.static(path.join(__dirname, 'build')));

app.use('/test', async function(req, res)
{
    try
    {
        console.log('Test');
        res.header("Content-type", "application/json");
        res.end(JSON.stringify(
        {
            error: false,
            notice: 'Test'
        }));
    }
    catch(error)
    {
        console.log(error);
    }
})

app.use('/fetchThousand', async function(req, res)
{
    try
    {
        res.header("Content-type", "application/json");
        res.end(JSON.stringify(
        {
            error: false,
            data: thousand,
            notice: 'Test'
        }));
    }
    catch(error)
    {
        console.log(error);
    }
})

app.use('/api/fetchThousand', async function(req, res)
{
    try
    {
      res.header("Content-type", "application/json");
      res.end(JSON.stringify(
      {
          error: false,
          data: thousand,
          notice: 'Test'
      }));
    }
    catch(error)
    {
        console.log(error);
    }
})

app.use('/fetchTenThousand', async function(req, res)
{
  try
  {
    res.header("Content-type", "application/json");
    res.end(JSON.stringify(
    {
        error: false,
        data: tenThousand,
        notice: 'Test'
    }));
  }
  catch(error)
  {
      console.log(error);
  }
})

app.use('/api/fetchTenThousand', async function(req, res)
{
  try
  {
    res.header("Content-type", "application/json");
    res.end(JSON.stringify(
    {
        error: false,
        data: tenThousand,
        notice: 'Test'
    }));
  }
  catch(error)
  {
      console.log(error);
  }
})


thousand = JSON.parse(fs.readFileSync('thousand.json', 'utf8'));
tenThousand = JSON.parse(fs.readFileSync('tenThousand.json', 'utf8'));

app.listen(port, () => console.log(`JS-framework-comparison app listening on port ${port}!`))
