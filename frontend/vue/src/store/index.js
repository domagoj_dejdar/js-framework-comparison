import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'
Vue.use(VueAxios, axios);
axios.defaults.headers.post['Content-Type'] = 'multipart/form-data;';

Vue.use(Vuex);
import moment from 'moment-timezone'

export default new Vuex.Store({
state: {
    //initial data for the application
    activities: [
      {
        name: 'Hand in thesis',
        completed: true
      },
      {
        name: 'Prepare thesis defense',
        completed: true
      },
      {
        name: 'Obtain title "stručni specijalist inženjer informacijske tehnologije"',
        completed: false
      }
    ],
    actionResponse: {
      error: false,
      notice: '',
      success: false
    }
  },
  getters: {
    activities: state => state.activities,
    actionResponse: state => state.actionResponse
  },
  actions: {
    async addActivity({commit}, activity)
    {
      var response = {};

      if(activity.name == '' || activity.name.trim().length < 3)
      {
        response = {
          error: true,
          notice: 'Please enter the activity name with at least 3 characters.',
          success: false,
          inputError: true
        }
      }
      else
      {
        await commit('addActivity', activity)

        response = {
          error: false,
          notice: 'Entered new activity!',
          success: true
        };
      }

      commit('toggleResponse', response)
    },
    async deleteActivity({commit}, activity)
    {
      await commit('deleteActivity', activity)
      commit('toggleResponse', {error: false, notice: 'Activity deleted.', success: true});
    },
    changeActivityState({commit}, activity)
    {
      commit('changeActivityState', activity)
    },
    async generateActivities({commit, state}, num)
    {
      if(num != null)
      {
        var len = 0;

        if(num == 'fetch_1000')
        {
          await commit('fetchActivities', {url: 'fetchThousand'})
          commit('toggleResponse', {error: false, notice: 'Fetched 1000 activities.', success: true});
        }
        if(num == 'fetch_10000')
        {
          await commit('fetchActivities', {url: 'fetchTenThousand'})
          commit('toggleResponse', {error: false, notice: 'Fetched 10000 activities.', success: true});
        }
        else if(num == 0)
        {
          state.activities = [];
          commit('toggleResponse', {error: false, notice: 'Activities have been reset.', success: true});
        }
        else if(num > 0)
        {
          len = state.activities.length;
          var numTemp = num;

          while(numTemp > 0)
          {
            var activity = {
              name: 'Test activity ' + len++,
              completed: false
            };

            await commit('addActivity', activity);

            numTemp--;
          }
          commit('toggleResponse', {error: false, notice: 'Added ' + num + ' activities.', success: true});
        }
        else if(num < 0)
        {
          num = num * (-1);
          len = state.activities.length;
          var start = 0;

          if(num > len) num = len;
          else
          {
            start = len - num;
          }
          if(num > 0)
          {
            await commit('deleteActivities', {num: num, start: start});
            commit('toggleResponse', {error: false, notice: 'Deleted ' + num + ' activities.', success: true});
          }
          else
          {
            commit('toggleResponse', {error: true, notice: 'Activity list is empty, nothing to delete!', success: false});
          }
        }
      }
    },
    clearAlert({commit, state})
    {
      commit('toggleResponse', null);
    }
  },
  mutations: {
    async fetchActivities(state, option)
    {
      await axios.post('http://localhost:8000/'+option.url).then((response) => {
        state.activities = state.activities.concat(response.data.data);
      });
    },
    addActivity(state, activity)
    {
      state.activities.push(activity);
    },
    deleteActivity(state, activity)
    {
      state.activities = state.activities.filter((val) => val !== activity);
    },
    deleteActivities(state, params)
    {
      state.activities = state.activities.splice(params.num, params.start);
    },
    changeActivityState(state, activity)
    {
      state.activities.map((val)=> {
          if(val.name === activity.name ) {
              const aux = !val.completed;
              val.completed = aux;
          }
          return val;
      })
    },
    toggleResponse(state, response)
    {
      this._vm.$nextTick(() =>
      {
        state.actionResponse = (response != null) ? response :   state.actionResponse = {error: false, notice: '', success: false};
      });
    }
  }
})
