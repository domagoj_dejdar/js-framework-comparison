import Vue from 'vue'
import Router from 'vue-router'



// Views
import Main from '@/views/Main'

Vue.use(Router)

export default new Router({
	//mode: 'hash',
	//linkActiveClass: 'open active',
	//scrollBehavior: () => ({ y: 0 }),
	mode: 'history',
	routes:
	[
		{
      path: '*',
      name: 'home',
      component: Main,
    }
	]
})
