module.exports = {
  devServer: {
    proxy:
    {
      "/*":
      {
        target: 'http://localhost:8000',
        changeOrigin: true,
        cookieDomainRewrite: ""
      }
    }
  },
  configureWebpack: {
    devtool: 'source-map'
  },
	baseUrl: '/',
  outputDir: '../../backend/build/vue/'
}
