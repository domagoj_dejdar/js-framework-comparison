import React, { Component } from 'react'

class ActivityCrud extends Component {
  constructor(props) {
      super(props)
      this.state = {
        name: '',
        completed: false
      }
  }

  componentWillReceiveProps(props) {
    if(props.response.inputError !== undefined && props.response.success === true) this.resetForm();
  }

  changeName = e => {
    const name = e.target.value
    this.setState({
      name: name
    })
  }

  changeStatus = e => {
    const completed = !this.state.completed
    this.setState({
      completed: completed
    })
  }

  resetForm = e => {
    this.setState(state => ({
      name: '',
      completed: false
    }));
  }

  closeForm = e => {
    this.props.closeForm();
  }

  submitForm = e => {
    this.props.addItem({
      name: this.state.name,
      completed: this.state.completed
    })
  }

  render() {
    return (
      <div className="form-container">
        <form className="form" onSubmit={ e => { e.preventDefault() } }>
          <div className="form-input-container">
            <label className="form-label input-label">Name:</label>
            <input className={"text-input" + ((this.props.response.inputError && this.props.response.error) ? ' error' : '')} type="text" placeholder="Enter the activity name..." value={this.state.name} onChange={this.changeName} />
            <small className="input-description">Enter the desired activity name which will be shown on the Activities list.</small>
          </div>
          <div className="form-input-container checkbox-container">
            <input className="form-checkbox" id="activityCompleted" type="checkbox" onChange={this.changeStatus} checked={this.state.completed} />
            <label className="form-label checkbox-label" htmlFor="activityCompleted">Activity Completed</label>
          </div>
          <div className="form-input-container actions-container">
            <div className="button-container">
              <input type="submit" className="button-primary primary" value="Save" onClick={this.submitForm} />
            </div>
            <div className="button-container">
              <input type="submit" className="button-primary danger" value="Cancel" onClick={this.closeForm} />
            </div>
          </div>
        </form>
      </div>
    )
  }
}

export default ActivityCrud
