import React, { Component } from 'react'

class ActivityCrud extends Component {
  constructor() {
      super()
      this.state = {
        formData: {
          name: '',
          completed: false
        }
      }
  }

  changeName = e => {
    const name = e.target.value
    this.setState({
      formData: {
        name: name,
        completed: this.state.formData.completed
      }
    })
  }

  changeStatus = e => {
    const completed = !this.state.formData.completed
    this.setState({
      formData: {
        name: this.state.formData.name,
        completed: completed
      }
    })
  }

  resetForm = e => {
    this.setState(state => ({
      formData: {
        name: '',
        completed: false
      }
    }));
  }

  dummy = e => {
    return
  }

  render() {
    return (
      <div className="form-container">
        <form className="form" onSubmit={ e => { e.preventDefault() } }>
          <div className="form-input-container">
            <label className="form-label input-label">Name:</label>
            <input className="text-input" type="text" placeholder="Enter the activity name..." value={this.state.formData.name} onChange={this.changeName} />
            <small className="input-description">Enter the desired activity name which will be shown on the Activities list.</small>
          </div>
          <div className="form-input-container checkbox-container">
            <input className="form-checkbox" id="activityCompleted" type="checkbox" onChange={this.changeStatus} checked={this.state.formData.completed} />
            <label className="form-label checkbox-label" htmlFor="activityCompleted">Activity Completed</label>
          </div>
          <div className="form-input-container actions-container">
            <div className="button-container">
              <input type="submit" className="button-primary primary" value="Save" onClick={this.props.addItem} />
            </div>
            <div className="button-container">
              <input type="submit" className="button-primary danger" value="Cancel" onClick={this.resetForm} />
            </div>
          </div>
        </form>
      </div>
    )
  }
}

export default ActivityCrud
