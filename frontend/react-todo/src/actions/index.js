export const addTodo = data => ({
  type: 'ADD_TODO',
  data
})

export const addTodos = data => ({
  type: 'ADD_TODOS',
  data
})

export const toggleTodo = todo => ({
  type: 'TOGGLE_TODO',
  todo
})

export const deleteTodo = todo => ({
  type: 'DELETE_TODO',
  todo
})

export const deleteTodos = data => ({
  type: 'DELETE_TODOS',
  data
})

export const clearAlert = val => ({
  type: 'TOGGLE_RESPONSE',
  val: null
})

export const toggleResponse = val => ({
  type: 'TOGGLE_RESPONSE',
  val: val
})

export const resetTodos = () => ({
  type: 'RESET_TODOS'
})
