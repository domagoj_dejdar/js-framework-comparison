import React, { Component } from 'react'
import '../assets/css/App.css'
import Header from './Header'
import ActivityCrud from '../containers/ActivityCrud'
import VisibleTodoList from '../containers/VisibleTodoList'
import {connect} from 'react-redux'
import { addTodo, clearAlert, toggleResponse, resetTodos, deleteTodos, addTodos } from '../actions'
import axios from 'axios'

class App extends Component {
  constructor() {
    super()
    this.state = {
      menuOpen: '',
      special: '',
      specials:
      [
        {
          text: 'Please select an option...',
          value: ''
        },
        {
          text: 'Add 100',
          value: 100
        },
        {
          text: 'Add 1000',
          value: 1000
        },
        {
          text: 'Remove 100',
          value: -100
        },
        {
          text: 'Remove 1000',
          value: -1000
        },
        {
          text: 'Fetch 1.000',
          value: 'fetch_1000'
        },
        {
          text: 'Fetch 10.000',
          value: 'fetch_10000'
        },
        {
          text: 'Delete All',
          value: 0
        }
      ]
    }
  }

  toggleMenu = e => {
    const menu = (e !== undefined && e.target !== undefined && e.target.id !== undefined) ? e.target.id : ''
    this.setState(state => ({
      menuOpen: (menu !== this.state.menuOpen) ? menu : '',
      special: ''
    }));
  }

  addItem = (data) => {
    var response = {
      error: false,
      notice: '',
      success: false
    }
    if(data.name.length < 3)
    {
      response = {
        error: true,
        notice: 'Please enter the activity name with at least 3 characters.',
        success: false,
        inputError: true
      }
    }
    else
    {
      this.props.addTodo(data);
      response = {
        error: false,
        notice: 'Entered new activity!',
        success: true,
        inputError: false
      }
    }

    this.props.toggleResponse(response);
  }

  clearAlert = () => {
    this.props.clearAlert(null)
  }

  changeSpecial = e => {
    const special = e.target.value
    this.setState(state => ({
      special: special
    }));
  }

  specialCancel = () => {
    this.toggleMenu();
  }

  fetchActivities= (url) =>
  {
    var activityNum = (url == 'fetchThousand') ? '1.000' : '10.000';

    axios.get('http://localhost:8000/'+url).then(res =>
    {
      this.props.addTodos(res.data.data)
      this.props.toggleResponse({error: false, notice: 'Fetched '+activityNum+' activities.', success: true});
    });
  }

  specialSubmit = () => {
    if(this.state.special !== '')
    {
      var len = 0,
        numTemp = 0;

      if(this.state.special === 'fetch_1000')
      {
        this.fetchActivities('fetchThousand')
      }
      if(this.state.special === 'fetch_10000')
      {
        this.fetchActivities('fetchTenThousand')
      }
      else if(this.state.special === '0')
      {
        this.props.resetTodos();
        this.props.toggleResponse({error: false, notice: 'Activities have been reset.', success: true});
      }
      else if(this.state.special > 0)
      {
        len = this.props.todos.length;
        numTemp = this.state.special;

        while(numTemp > 0)
        {
          var todo = {
            name: 'Test activity ' + len++,
            completed: false
          };

          this.props.addTodo(todo);
          numTemp--;
        }
        this.props.toggleResponse({error: false, notice: 'Added ' + this.state.special + ' activities.', success: true});
      }
      else if(this.state.special < 0)
      {
        var start = this.state.special * (-1);
        len = this.props.todos.length;
        numTemp = 0;

        if(start > len) start = len;
        else
        {
          numTemp = len - start;
        }
        if(start > 0)
        {
          this.props.deleteTodos({num: numTemp, start: start});
          this.props.toggleResponse({error: false, notice: 'Deleted ' + start + ' activities.', success: true});
        }
        else
        {
          this.props.toggleResponse({error: true, notice: 'Activity list is empty, nothing to delete!', success: false});
        }
      }
    }
  }

  render() {
    return (
      <div>
        <Header />
        <div className="main-container">
          <div className="app-container">
            {((this.props.actionResponse.error && !this.props.actionResponse.success) || (!this.props.actionResponse.error && this.props.actionResponse.success)) &&
            <div className={"notification-dialog" + (this.props.actionResponse.error ? ' error' : ' success')}>
                <button type="button" aria-label="Close" className="close" onClick={this.clearAlert}>×</button>
                {this.props.actionResponse.notice}
            </div>
            }
            <div className="menu-container">
              <div className="menu-options" onSubmit={ e => { e.preventDefault() } }>
                <div className="option-toggle">
                  <input type="submit" className="button-primary primary" onClick={this.toggleMenu} id="activityCrud" value="Add New" />
                </div>
                <div className="option-toggle">
                  <input type="submit" className="button-primary primary" onClick={this.toggleMenu} id="specialOptions" value="Special" />
                </div>
              </div>
              {this.state.menuOpen === 'activityCrud' &&
              <div className="submenu-container">
                <div className="submenu">
                  <ActivityCrud response={this.props.actionResponse} addItem={this.addItem} clearAlert={this.clearAlert} closeForm={this.toggleMenu}/>
                </div>
              </div>
              }
              {this.state.menuOpen === 'specialOptions' &&
              <div className="submenu-container">
                <div className="submenu">
                  <div className="form-container">
                    <form action="" className="form" onSubmit={ e => { e.preventDefault() } }>
                      <div className="form-input-container">
                        <label className="form-label input-label">Options:</label>
                        <div className="select-container">
                          <select className="form-select" value={this.state.special} onChange={this.changeSpecial}>
                            {this.state.specials.map((special, index) =>
                              <option className="form-option" value={special.value} key={index}>
                                {special.text}
                              </option>
                            )}
                          </select>
                        </div>
                      </div>
                      <div className="form-input-container actions-container">
                        <div className="button-container">
                          <input type="submit" className="button-primary primary" value="Save" onClick={this.specialSubmit} />
                        </div>
                        <div className="button-container">
                          <input type="submit" className="button-primary danger" value="Cancel" onClick={this.specialCancel} />
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              }
            </div>
            <VisibleTodoList />
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    actionResponse : state.actionResponse,
    todos: state.todos
  }
}

const mapDispatchToProps = dispatch => ({
  addTodo: data => dispatch(addTodo(data)),
  addTodos: data => dispatch(addTodos(data)),
  clearAlert: id => dispatch(clearAlert(id)),
  toggleResponse: id => dispatch(toggleResponse(id)),
  resetTodos: id => dispatch(resetTodos(id)),
  deleteTodos: id => dispatch(deleteTodos(id))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)

//export default App
