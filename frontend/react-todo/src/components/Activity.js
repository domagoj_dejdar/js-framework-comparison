import React from 'react'
import PropTypes from 'prop-types'
import checkedIcon from '../assets/img/checked.svg'
import uncheckedIcon from '../assets/img/unchecked.svg'
import deleteIcon from '../assets/img/delete.svg'

const Activity = ({ changeStatus, deleteActivity, completed, name }) => (
  <div className="activity">
    <div className="activity-details">
      <div className="activity-checkbox delete" onClick={deleteActivity}>
        <img className="activity-checkbox-image" src={deleteIcon} alt="completed icon" />
      </div>
      <div className="activity-name">{name}</div>
      <div className="activity-checkbox change-state" onClick={changeStatus}>
        {completed &&
          <img className="activity-checkbox-image" src={checkedIcon} alt="completed icon" />
        }
        {!completed &&
          <img className="activity-checkbox-image" src={uncheckedIcon} alt="completed icon" />
        }
      </div>
    </div>
  </div>
)

Activity.propTypes = {
  changeStatus: PropTypes.func.isRequired,
  deleteActivity: PropTypes.func.isRequired,
  completed: PropTypes.bool.isRequired,
  name: PropTypes.string.isRequired
}

export default Activity
