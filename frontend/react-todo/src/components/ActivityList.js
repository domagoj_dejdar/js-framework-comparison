import React from 'react'
import PropTypes from 'prop-types'
import Activity from './Activity'

const ActivityList = ({ todos, toggleTodo, deleteTodo }) => (
  <div className="activity-list-container">
    <div className="activity-list-details">
      <h3 className="list-title">ToDo List</h3>
      <div className="activity-stats-container">
        <div className="activity-stat">
          Total: {todos.length}
        </div>
        <div className="activity-stat">
          Completed: {todos.filter((val) => val.completed === true).length}
        </div>
      </div>
    </div>
    {todos.length > 0 &&
      <div className="activity-list">
      {todos.map((todo, index) =>
        <Activity
          key={index}
          {...todo}
          changeStatus={() => toggleTodo(todo)}
          deleteActivity={() => deleteTodo(todo)}
        />
      )}
      </div>
    }
  </div>
)

ActivityList.propTypes = {
  todos: PropTypes.arrayOf(PropTypes.shape({
    completed: PropTypes.bool.isRequired,
    name: PropTypes.string.isRequired
  }).isRequired).isRequired,
  toggleTodo: PropTypes.func.isRequired,
  deleteTodo: PropTypes.func.isRequired
}

export default ActivityList
