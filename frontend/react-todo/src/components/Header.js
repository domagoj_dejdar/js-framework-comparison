import React from 'react'
import logo from '../assets/img/logo.svg'

const Footer = () => (
  <header>
    <div className="header-container">
      <div className="logo-container">
        <img className="logo" alt="React Logo" src={logo} />
      </div>
      <div className="title-container">
        <h3>React ToDo App</h3>
      </div>
    </div>
  </header>
)

export default Footer
