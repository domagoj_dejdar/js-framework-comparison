import { combineReducers } from 'redux'
import todos from './todos'
import actionResponse from './actionResponse'

export default combineReducers({
  todos,
  actionResponse
})
