const actionResponse = (state = {error: false, notice: '', success: false}, action) => {
  switch (action.type) {
    case 'TOGGLE_RESPONSE':
      return (action.val == null) ? {error: false, notice: '', success: false} : action.val
    case 'DELETE_TODO':
      return {error: false, notice: 'Activity deleted.', success: true}
    default:
      return state
  }
}

export default actionResponse
