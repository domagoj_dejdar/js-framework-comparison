const todos = (state = [{name: 'Hand in thesis', completed: true},{name: 'Prepare thesis defense', completed: true},{name: 'Obtain title "stručni specijalist inženjer informacijske tehnologije"', completed: false}], action) => {
  switch (action.type) {
    case 'ADD_TODO':
      return [
        ...state,
        {
          name: action.data.name,
          completed: action.data.completed
        }
      ]
    case 'ADD_TODOS':
      return state.concat(action.data)
    case 'DELETE_TODOS':
      return state.splice(action.data.start, action.data.num)
    case 'TOGGLE_TODO':
      return state.map(todo =>
        (todo === action.todo)
          ? {...todo, completed: !todo.completed}
          : todo
      )
    case 'DELETE_TODO':
      return state.filter((val) => val !== action.todo)
    case 'RESET_TODOS':
      return []
    case 'GENERATE_ACTIVITIES':
      return state
    default:
      return state
  }
}

export default todos
