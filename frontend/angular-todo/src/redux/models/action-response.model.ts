export interface ActionResponse {
  error: boolean;
  notice: string;
  success: boolean;
  inputError: boolean;
}
