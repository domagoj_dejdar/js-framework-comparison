import { Action } from '@ngrx/store';
import { ActionResponse } from '../models/action-response.model';

export const TOGGLE_RESPONSE = '[ACTIONRESPONSE] toggle';
export const DELETE_TODO = '[TODO] delete';

export class DeleteTodoResponseAction implements Action {
  readonly type = DELETE_TODO;

  constructor() {}
}

export class ToggleAction implements Action {
  readonly type = TOGGLE_RESPONSE;

  constructor(
    public response: ActionResponse
  ) {}
}

export type ActionResponseActionType =
ToggleAction |
DeleteTodoResponseAction
