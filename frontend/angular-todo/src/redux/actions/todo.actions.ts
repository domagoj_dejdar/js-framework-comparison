import { Action } from '@ngrx/store';
import { Todo } from '../models/todo.model';

export const ADD_TODO = '[TODO] add';
export const ADD_TODOS = '[TODO] add multiple';
export const DELETE_TODO = '[TODO] delete';
export const DELETE_TODOS = '[TODO] delete multiple';
export const TOGGLE_TODO = '[TODO] toggle';
export const RESET_TODOS = '[TODO] clear all';
export const GENERATE_ACTIVITIES = '[TODO] generate activities';

export class AddTodoAction implements Action {
  readonly type = ADD_TODO;

  constructor(
    public name: string,
    public completed: boolean
  ) {}
}

export class AddTodosAction implements Action {
  readonly type = ADD_TODOS;

  constructor(
    public data: Array<Todo>
  ) {}
}

export class DeleteTodoAction implements Action {
  readonly type = DELETE_TODO;

  constructor(
    public todo: Todo
  ) {}
}

export class DeleteTodoSAction implements Action {
  readonly type = DELETE_TODOS;

  constructor(
    public num: number,
    public start: number
  ) {}
}

export class ToggleTodoAction implements Action {
  readonly type = TOGGLE_TODO;

  constructor(
    public todo: Todo
  ) {}
}

export class ResetTodosAction implements Action {
  readonly type = RESET_TODOS;
}

export class GenerateActivitiesAction implements Action {
  readonly type = GENERATE_ACTIVITIES;
}

export type TodoActionType =
AddTodoAction |
AddTodosAction |
ToggleTodoAction |
DeleteTodoAction |
ResetTodosAction |
GenerateActivitiesAction |
DeleteTodoSAction;
