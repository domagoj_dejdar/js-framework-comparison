import { Todo } from '../models/todo.model';
import * as TodoActions from '../actions/todo.actions';

const initialState: Todo[] = [{name: 'Hand in thesis', completed: true},{name: 'Prepare thesis defense', completed: true},{name: 'Obtain title "stručni specijalist inženjer informacijske tehnologije"', completed: false}];

export function TodosReducer(state: Todo[] = initialState, action: TodoActions.TodoActionType) {
  switch (action.type) {
    case TodoActions.ADD_TODO: {
      return [
        ...state,
        {
          name: action.name,
          completed: action.completed
        }
      ];
    }
    case TodoActions.ADD_TODOS: {
      return state.concat(action.data);
    }
    case TodoActions.TOGGLE_TODO: {
      return state.map(todo =>
        (todo === action.todo)
          ? {...todo, completed: !todo.completed}
          : todo
      );
    }
    case TodoActions.DELETE_TODO: {
        return state.filter((val) => val !== action.todo);
    }
    case TodoActions.DELETE_TODOS: {
        return state.splice(action.start, action.num)
    }
    case TodoActions.RESET_TODOS: {
        return [];
    }
    case TodoActions.GENERATE_ACTIVITIES: {
      return state;
    }
    default: {
      return state;
    }
  }
}
