import { ActionResponse } from '../models/action-response.model';
import * as ActionResponseActions from '../actions/action-response.actions';

const initialState: ActionResponse = {error: false, notice: '', success: false, inputError: false};

export function ActionResponseReducer(state: ActionResponse = initialState, action: ActionResponseActions.ActionResponseActionType) {
  switch (action.type) {
    case ActionResponseActions.TOGGLE_RESPONSE: {
        return (action.response == null) ? {error: false, notice: '', success: false, inputError: false} : action.response;
    }
    case ActionResponseActions.DELETE_TODO: {
      return {error: false, notice: 'Activity deleted.', success: true, inputError: false};
    }
    default: {
      return state;
    }
  }
}
