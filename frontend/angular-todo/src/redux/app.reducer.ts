import { ActionReducerMap } from '@ngrx/store';

import { TodosReducer } from './reducers/todo.reducer';
import { Todo } from './models/todo.model';
import { ActionResponseReducer } from './reducers/action-response.reducer';
import { ActionResponse } from './models/action-response.model';

export interface AppState {
  todos: Todo[];
  actionResponse: ActionResponse;
}

export const rootReducer: ActionReducerMap<AppState> = {
  todos: TodosReducer,
  actionResponse: ActionResponseReducer
};
