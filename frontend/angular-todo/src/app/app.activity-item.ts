import { Component, OnInit, Input } from '@angular/core';
import { Todo } from './../redux/models/todo.model';
import * as TodoActions from './../redux/actions/todo.actions';
import { Store } from '@ngrx/store';
import { AppState } from './../redux/app.reducer';

//define base component configuration
@Component({
  selector: 'activity-item',
  templateUrl: './app.activity-item.html'
})

export class ActivityItem implements OnInit {
  //define base component data and map data received from parent component
  title = 'activity-item';
  @Input() activity : Todo;

  constructor(
    //define connection to global store
    private store: Store<AppState>
  ) { }

  ngOnInit()
  {
    //define extra functions and actions that should happen when the component initializes
  }

  changeActivityState()
  {
    //invoke store action for changing activity completion status
    this.store.dispatch(new TodoActions.ToggleTodoAction(this.activity));
  }

  deleteActivity()
  {
    //invoke store action for deleting the activity from the list
    this.store.dispatch(new TodoActions.DeleteTodoAction(this.activity));
  }
}
