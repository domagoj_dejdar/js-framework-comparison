import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { AppState } from './../redux/app.reducer';
import { Todo } from './../redux/models/todo.model';

//define base component configuration
@Component({
  selector: 'activity-list',
  templateUrl: './app.activity-list.html'
})

export class ActivityList implements OnInit {
  //define base component data
  title = 'activity-list';
  activities$: Todo[];
  completed$: number = 0;
  total$: number = 0;

  constructor(public store: Store<AppState>)
  {
    //subscribe to changes in the "todosList" store data
    store.pipe(select('todos')).subscribe(todosList => {
      //add values to previously defined component data
      this.activities$ = todosList;
      this.total$ = todosList.length;
      this.completed$ = todosList.filter((val: Todo) => val.completed === true).length;
      //all values defined here will be updated when the data inside "todosList" changes (updates)
    });
  }

  ngOnInit()
  {
    //define extra functions and actions that should happen when the component initializes
  }
}
