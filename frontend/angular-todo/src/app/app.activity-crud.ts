import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';

import { AppState } from './../redux/app.reducer';
import * as ActionResponseActions from './../redux/actions/action-response.actions';
import { ActionResponse } from '../redux/models/action-response.model';

@Component({
  selector: 'activity-crud',
  templateUrl: './app.activity-crud.html'
})

export class ActivityCrud implements OnInit {

  @Output() closeCrud = new EventEmitter<string>();
  @Output() formAction = new EventEmitter<Object>();

  title = 'activity-crud';

  textField: FormControl;
  completedField: FormControl;

  constructor(
    private store: Store<AppState>
  ) {
    this.textField = new FormControl('', [Validators.required]);
    this.completedField = new FormControl('', [Validators.required]);
  }

  ngOnInit()
  {

  }

  formSubmit()
  {

    let formData: Object = {};

    if(this.textField.valid && this.textField.value.trim().length > 3)
    {
      formData = {
        name: this.textField.value,
        completed: (this.completedField.value == true) ? this.completedField.value : false
      };

      this.textField.setValue('', { emitEvent: false });
      this.completedField.setValue('', { emitEvent: false });
    }

    this.formAction.emit(formData);
  }

  formCancel()
  {
    this.closeCrud.emit();
    const actResObj: ActionResponse = null;
    const toggleNotification = new ActionResponseActions.ToggleAction(actResObj);
    this.store.dispatch(toggleNotification);
  }
}
