import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { rootReducer } from './../redux/app.reducer';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { AppRoot } from './app.app';
import { AppMain } from './app.main';
import { AppHeader } from './app.header';
import { ActivityCrud } from './app.activity-crud';
import { ActivityList } from './app.activity-list';
import { ActivityItem } from './app.activity-item';

@NgModule({
  declarations: [
    AppHeader,
    AppRoot,
    AppMain,
    ActivityCrud,
    ActivityList,
    ActivityItem
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forRoot(rootReducer),
    StoreDevtoolsModule.instrument({
      maxAge: 10
    })
  ],
  providers: [],
  bootstrap: [AppRoot]
})
export class AppModule { }
