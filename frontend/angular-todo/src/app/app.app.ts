import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.app.html'
})
export class AppRoot {
  title = 'angular-todo';
}
