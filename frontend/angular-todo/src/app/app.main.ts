import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { AppState } from './../redux/app.reducer';
import { Todo } from './../redux/models/todo.model';
import { ActionResponse } from './../redux/models/action-response.model';
import * as ActionResponseActions from './../redux/actions/action-response.actions';
import * as TodoActions from './../redux/actions/todo.actions';
import axios from 'axios'
axios.defaults.headers.get['Content-Type'] = 'multipart/form-data;';
axios.defaults.headers.get['Access-Control-Allow-Origin'] = '*';

@Component({
  selector: 'app-main',
  templateUrl: './app.main.html'
})
export class AppMain implements OnInit {
  title = 'angular-todo';

  menuOpen = '';

  todos$: Todo[];
  actionResponse$: ActionResponse;

  special$: any = null;

  specials$: Array<Object> = [
    {
      text: 'Please select an option...',
      value: null
    },
    {
      text: 'Add 100',
      value: 100
    },
    {
      text: 'Add 1000',
      value: 1000
    },
    {
      text: 'Remove 100',
      value: -100
    },
    {
      text: 'Remove 1000',
      value: -1000
    },
    {
      text: 'Fetch 1.000',
      value: 'fetch_1000'
    },
    {
      text: 'Fetch 10.000',
      value: 'fetch_10000'
    },
    {
      text: 'Delete All',
      value: 0
    }
  ];

  constructor(public store: Store<AppState>)
  {
    store.pipe(select('actionResponse')).subscribe(val => this.actionResponse$ = val);
    store.pipe(select('todos')).subscribe(val => this.todos$ = val);
  }

  ngOnInit()
  {
    //console.log(this._store);
  }

  toggleMenu(option: any)
  {
    if(option == undefined || option == this.menuOpen) this.menuOpen = '';
    else this.menuOpen = option;

    this.special$ = null;
  }

  clearAlert()
  {
    const actResObj: ActionResponse = null;
    this.store.dispatch(new ActionResponseActions.ToggleAction(actResObj));
  }

  selectSpecial(value: any)
  {
    this.special$ = value;
  }

  specialCancel()
  {
    this.toggleMenu(undefined);
    this.special$ = null;
  }

  toggleResponse(responseDataIn: any)
  {
    let responseData: ActionResponse = responseDataIn;
    this.store.dispatch(new ActionResponseActions.ToggleAction(responseData));
  }

  formAction(data: any)
  {
    let responseData: ActionResponse = null;

    if(data.name != undefined && data.completed != undefined)
    {
      this.store.dispatch(new TodoActions.AddTodoAction(data.name, data.completed));

      responseData = {
        error: false,
        notice: 'Entered new activity!',
        success: true,
        inputError: false
      };
    }
    else
    {
      responseData = {
        error: true,
        notice: 'Please enter the activity name with at least 3 characters.',
        success: false,
        inputError: true
      };
    }

    this.toggleResponse(responseData);
  }

  fetchActivities(url: string)
  {
    var activityNum = (url == 'fetchThousand') ? '1.000' : '10.000';
    axios.get('http://localhost:8000/api/'+url).then(res =>
    {
      this.store.dispatch(new TodoActions.AddTodosAction(res.data.data));
      this.toggleResponse({error: false, notice: 'Fetched '+activityNum+' activities.', success: true});
    });
  }

  specialSubmit()
  {
    if(this.special$ !== '')
    {
      var len = 0,
        numTemp = 0;

      if(this.special$ === 'fetch_1000')
      {
        this.fetchActivities('fetchThousand')
      }
      else if(this.special$ === 'fetch_10000')
      {
        this.fetchActivities('fetchTenThousand')
      }
      else if(this.special$ === '0')
      {
        this.store.dispatch(new TodoActions.ResetTodosAction());
        this.toggleResponse({error: false, notice: 'Activities have been reset.', success: true});
      }
      else if(this.special$ > 0)
      {
        len = this.todos$.length;
        numTemp = this.special$;

        while(numTemp > 0)
        {
          var todo = {
            name: 'Test activity ' + len++,
            completed: false
          };

          this.store.dispatch(new TodoActions.AddTodoAction(todo.name, todo.completed));
          numTemp--;
        }
        this.toggleResponse({error: false, notice: 'Added ' + this.special$ + ' activities.', success: true});
      }
      else if(this.special$ < 0)
      {
        var start = this.special$ * (-1);
        len = this.todos$.length;
        numTemp = 0;

        if(start > len) start = len;
        else
        {
          numTemp = len - start;
        }
        if(start > 0)
        {
          this.store.dispatch(new TodoActions.DeleteTodoSAction(numTemp, start));
          this.toggleResponse({error: false, notice: 'Deleted ' + start + ' activities.', success: true});
        }
        else
        {
          this.toggleResponse({error: true, notice: 'Activity list is empty, nothing to delete!', success: false});
        }
      }
    }
  }
}
